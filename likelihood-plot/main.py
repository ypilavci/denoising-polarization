import numpy as np
import scipy
import matplotlib.pyplot as plt
import quaternion  # load the quaternion module
import bispy as bsp
import torch
import tqdm
from sklearn.preprocessing import normalize

# time vector
N = 2048 # length of the signal
t = np.linspace(0, 100, N)
dt = t[1]-t[0]
# ellipse parameters 
theta1 = np.pi/4  -t
chi1 = np.pi/6  # -t/10
phi1 = np.pi/6
f0 = 100/N/dt 
env = 1.0# bsp.utils.windows.hanning(N)
x1 = bsp.signals.bivariateAMFM(env, theta1, chi1, 2*np.pi*f0*t+ phi1)
x1_arr = quaternion.as_float_array(x1)

ts = 105


S0 = np.norm(x1)[ts]
S = bsp.utils.StokesNorm(x1)
S = quaternion.as_float_array(S)
S1 = S[ts,2]
S2 = S[ts,3]
S3 = S[ts,1]


NRep = 100
NoiseRep = 1000
Sigma_REP = 100
Sigma_exp_range =  np.linspace(start=-3, stop=3, num=Sigma_REP)

score_L1 = np.zeros([Sigma_REP,NRep])
score_L2 = np.zeros([Sigma_REP,NRep])
score_L3 = np.zeros([Sigma_REP,NRep]) 

score_N1 = np.zeros([Sigma_REP,NRep])
score_N2 = np.zeros([Sigma_REP,NRep])
score_N3 = np.zeros([Sigma_REP,NRep])

for exprep in tqdm.tqdm(range(NRep)): # For each experiment trial
    for (idx,sigma_exp) in enumerate((Sigma_exp_range)): # For each sigma
        sigma = 10**sigma_exp
        scale = 2*(sigma**2)
        std = 2*sigma*np.sqrt(S0)

        S0_emp = np.zeros([NoiseRep]) 
        S1_emp = np.zeros([NoiseRep]) 
        S2_emp = np.zeros([NoiseRep]) 
        S3_emp = np.zeros([NoiseRep]) 

        for i in range(NoiseRep): # For each noise realization 

            ## Hilbert transform of the noise given to j and k components
            n = np.zeros([N,4])
            noise_complex = np.random.randn(N,2)*sigma
            uH = np.imag(scipy.signal.hilbert(noise_complex[:,0]))
            vH = np.imag(scipy.signal.hilbert(noise_complex[:,1]))
            n[:,0] = noise_complex[:,0]
            n[:,1] = noise_complex[:,1]
            n[:,2] = uH
            n[:,3] = vH

            n = quaternion.from_float_array(n)
            x = (x1 + n) 

            S0_emp[i] = np.norm(x[ts])

            S_emp = bsp.utils.StokesNorm(x)
            S_emp = quaternion.as_float_array(S_emp)
            S1_emp[i] = S_emp[ts,2]
            S2_emp[i] = S_emp[ts,3]
            S3_emp[i] = S_emp[ts,1]

        score_L1[idx,exprep] = scipy.stats.kstest(S1_emp,scipy.stats.laplace(S1,scale).cdf).pvalue
        score_N1[idx,exprep] = scipy.stats.kstest(S1_emp,scipy.stats.norm(S1,std).cdf).pvalue

        score_L2[idx,exprep] = scipy.stats.kstest(S2_emp,scipy.stats.laplace(S2,scale).cdf).pvalue
        score_N2[idx,exprep] = scipy.stats.kstest(S2_emp,scipy.stats.norm(S2,std).cdf).pvalue

        score_L3[idx,exprep] = scipy.stats.kstest(S3_emp,scipy.stats.laplace(S3,scale).cdf).pvalue
        score_N3[idx,exprep] = scipy.stats.kstest(S3_emp,scipy.stats.norm(S3,std).cdf).pvalue


plt.figure(figsize=[12,3])
plt.subplot(1,3,1)
plt.title("S1")
plt.plot(10**Sigma_exp_range,np.mean(score_L1,axis=1),label="Laplace")
ci = 1.96 * np.std(score_L1,axis=1)/np.sqrt(NRep)
plt.fill_between(10**Sigma_exp_range,(np.mean(score_L1,axis=1)-ci),(np.mean(score_L1,axis=1)+ci), alpha=.5)

plt.plot(10**Sigma_exp_range,np.mean(score_N1,axis=1),label="Gaussian")
ci = 1.96 * np.std(score_N1,axis=1)/np.sqrt(NRep)
plt.fill_between(10**Sigma_exp_range,(np.mean(score_N1,axis=1)-ci),(np.mean(score_N1,axis=1)+ci), alpha=.5)
plt.hlines(0.05,10**Sigma_exp_range[0],10**Sigma_exp_range[-1],'r',linestyles='--',label="p=0.05")

plt.xscale("log")
plt.ylabel("p-Value")
plt.xlabel("Noise Variance $\\sigma$")
plt.legend()

plt.subplot(1,3,2)
plt.title("S2")
plt.plot(10**Sigma_exp_range,np.mean(score_L2,axis=1),label="Laplace")
ci = 1.96 * np.std(score_L2,axis=1)/np.sqrt(NRep)
plt.fill_between(10**Sigma_exp_range,(np.mean(score_L2,axis=1)-ci),(np.mean(score_L2,axis=1)+ci), alpha=.5)

plt.plot(10**Sigma_exp_range,np.mean(score_N2,axis=1),label="Gaussian")
ci = 1.96 * np.std(score_N2,axis=1)/np.sqrt(NRep)
plt.fill_between(10**Sigma_exp_range,(np.mean(score_N2,axis=1)-ci),(np.mean(score_N2,axis=1)+ci), alpha=.5)
plt.hlines(0.05,10**Sigma_exp_range[0],10**Sigma_exp_range[-1],'r',linestyles='--',label="p=0.05")

plt.xscale("log")
plt.ylabel("p-Value")
plt.xlabel("Noise Variance $\\sigma$")
plt.legend()

plt.subplot(1,3,3)
plt.title("S3")
plt.plot(10**Sigma_exp_range,np.mean(score_L3,axis=1),label="Laplace")
ci = 1.96 * np.std(score_L3,axis=1)/np.sqrt(NRep)
plt.fill_between(10**Sigma_exp_range,(np.mean(score_L3,axis=1)-ci),(np.mean(score_L3,axis=1)+ci), alpha=.5)

plt.plot(10**Sigma_exp_range,np.mean(score_N3,axis=1),label="Gaussian")
ci = 1.96 * np.std(score_N3,axis=1)/np.sqrt(NRep)
plt.fill_between(10**Sigma_exp_range,(np.mean(score_N3,axis=1)-ci),(np.mean(score_N3,axis=1)+ci), alpha=.5)
plt.hlines(0.05,10**Sigma_exp_range[0],10**Sigma_exp_range[-1],'r',linestyles='--',label="p=0.05")

plt.xscale("log")
plt.ylabel("p-Value")
plt.xlabel("Noise Variance $\\sigma$")

plt.legend()
plt.tight_layout()
plt.savefig("sigma-vs-pvalue")