import numpy as np
import quaternion
import matplotlib.pyplot as plt
import bispy as bsp
from sklearn.preprocessing import normalize
from utils import plot_on_sphere
import pickle

## PLOT FOR THE RESULTS

with open('results.pkl', 'rb') as f:
	snr_noise = pickle.load(f)
	snr_p2 = pickle.load(f)
	snr_nostokes = pickle.load(f)
	snr_onlystokes = pickle.load(f)
	snr_kerreg = pickle.load(f)

plt.plot(np.mean(snr_noise,axis=1),np.mean(snr_p2,axis=1),label="$\mathbf{x}_{mix}$",marker="d")
ci = 1.96 * np.std(snr_p2,axis=1)/np.sqrt(snr_p2.shape[1])
plt.fill_between(np.mean(snr_noise,axis=1), (np.mean(snr_p2,axis=1)-ci), (np.mean(snr_p2,axis=1)+ci), color='b', alpha=.1)

plt.plot(np.mean(snr_noise,axis=1),np.mean(snr_nostokes,axis=1),label="No stokes reg.",marker="s")
ci = 1.96 * np.std(snr_nostokes,axis=1)/np.sqrt(snr_nostokes.shape[1])
plt.fill_between(np.mean(snr_noise,axis=1), (np.mean(snr_nostokes,axis=1)-ci), (np.mean(snr_nostokes,axis=1)+ci), color="orange", alpha=.1)

plt.plot(np.mean(snr_noise,axis=1),np.mean(snr_onlystokes,axis=1),label="Only Stokes reg.",marker="o")
ci = 1.96 * np.std(snr_onlystokes,axis=1)/np.sqrt(snr_onlystokes.shape[1])
plt.fill_between(np.mean(snr_noise,axis=1), (np.mean(snr_onlystokes,axis=1)-ci), (np.mean(snr_onlystokes,axis=1)+ci), color='g', alpha=.1)

plt.plot(np.mean(snr_noise,axis=1),np.mean(snr_kerreg,axis=1),label="$\mathbf{x}_{ker}$",marker="x")
ci = 1.96 * np.std(snr_kerreg,axis=1)/np.sqrt(snr_kerreg.shape[1])
plt.fill_between(np.mean(snr_noise,axis=1), (np.mean(snr_kerreg,axis=1)-ci), (np.mean(snr_kerreg,axis=1)+ci), color='r', alpha=.1)


plt.plot(np.mean(snr_noise,axis=1),np.mean(snr_noise,axis=1),label="no denoising",linestyle="--")

plt.ylabel("Output SNR")
plt.grid()
plt.xlabel("Input SNR")
plt.legend()
plt.tight_layout()

plt.savefig("denoising-perf.pdf")
plt.close()


N = 1024
t = np.linspace(0, 2*np.pi/4, 1024) # time vector
dt = t[1] - t[0]
theta1 = np.pi/4 - 2*t
chi1 = np.pi/16 - t
phi1 = 0 
f0 = 25/N/dt 
S0 = bsp.utils.windows.hanning(N)

x_quad = bsp.signals.bivariateAMFM(S0, theta1, chi1, 2*np.pi*f0*t+ phi1)


with open('example.pkl', 'rb') as f:
	x=pickle.load(f)
	y=pickle.load(f)
	x_stis=pickle.load(f)
	x_nostokes=pickle.load(f)
	x_onlystokes=pickle.load(f)
	x_kerreg=pickle.load(f)

bsp.utils.visual.plot2D(t,x_stis)
plt.savefig("denoised_via_all_terms.pdf")

bsp.utils.visual.plot2D(t,x_nostokes)
plt.savefig("denoised_via_no_stokes.pdf")

bsp.utils.visual.plot2D(t,x_onlystokes)
plt.savefig("denoised_via_no_signal_smoother.pdf")

bsp.utils.visual.plot2D(t,x_kerreg)
plt.savefig("denoised_via_kerreg.pdf")

ax = plt.figure().add_subplot(projection='3d')
plot_on_sphere(x_stis,ax,label="$\\mathbf{x}_{mix}$",t=t,scatter=True)
plot_on_sphere(x_quad,ax,label="True Signal",t=[0],scatter=False)

plt.legend()
plt.tight_layout()
plt.savefig("xmix_norm_stokes.pdf")
plt.close()
ax = plt.figure().add_subplot(projection='3d')

plot_on_sphere(x_kerreg,ax,label="$\\mathbf{x}_{ker}$",t=t,scatter=True)
plot_on_sphere(x_quad,ax,label="True Signal",t=[0],scatter=False)
plt.legend()
plt.tight_layout()
plt.savefig("xker_norm_stokes.pdf")
plt.close()
