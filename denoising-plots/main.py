import numpy as np
import scipy
import matplotlib.pyplot as plt
import quaternion  # load the quaternion module
import bispy as bsp
import torch
import tqdm
import time
from sklearn.preprocessing import normalize
from utils import STIS,optimize_loop,snr_bivariate,param_search,objective_STIS,objective_KReSP,KReSP
from ray import  tune,init
import pickle 

np.random.seed(5)
init(num_cpus=16)
N = 1024 # length of the signal
t = np.linspace(0, 2*np.pi/4, N) # time vector
dt = t[1]-t[0]

# ellipse parameters - AM-FM-PM polarized 
theta1 = np.pi/4 - 2*t
chi1 = np.pi/16 - t
phi1 = 0 
f0 = 25/N/dt 
S0 = bsp.utils.windows.hanning(N)

x = bsp.signals.bivariateAMFM(S0, theta1, chi1, 2*np.pi*f0*t+ phi1)
x = quaternion.as_float_array(x)[:,:2]
sigmaRange = np.linspace(0.01,0.5,10)
NREP =  5
snr_noise = np.zeros([len(sigmaRange),NREP])
snr_p2 = np.zeros([len(sigmaRange),NREP])
snr_nostokes = np.zeros([len(sigmaRange),NREP])
snr_onlystokes = np.zeros([len(sigmaRange),NREP])
snr_kerreg = np.zeros([len(sigmaRange),NREP])

for nrep in tqdm.tqdm(range(NREP)):
	s = 0 
	for sigma in (sigmaRange):
		n = np.random.randn(N,2)
		y = sigma*n + x # Noisy signal
		snr_noise[s,nrep] =  (snr_bivariate(x,y) )
		print("sigma: " + str(sigma) + " nrep: " + str(nrep) + " Noise SNR: "+  str(snr_bivariate(x,y) ) )

		search_space = {"x":tune.grid_search([x]),"t":tune.grid_search([t]),"y":tune.grid_search([y]),"lambdax": tune.grid_search((0.1)**np.linspace(5,15,7)), "lambdaS":  tune.grid_search((0.1)**np.linspace(5,10,7)) , "beta1":tune.grid_search((0.10)**np.linspace(-2,2,5)),"beta2":tune.grid_search((0.10)**np.linspace(-2,2,5)),"sigma2":tune.grid_search([sigma**2])}

		config = param_search(objective_STIS,search_space)
		
		model = STIS(t,y,lambdax=config["lambdax"],lambdaS=config["lambdaS"],beta1=config["beta1"],beta2=config["beta2"],sigma2=sigma**2,p=2)
#		model = STIS(t,y,lambdax=1e-8,lambdaS=1e-8,beta1=0.1,beta2=0.1,sigma2=sigma**2,p=2)

		optimizer = torch.optim.Adam(model.parameters(), lr=0.01)

		model = optimize_loop(model,optimizer,numit=1000)


		snr_p2[s,nrep] = (snr_bivariate(x,model.X.detach().numpy()))

		print("sigma: " + str(sigma) + " nrep: " + str(nrep) + " STIS SNR: "+  str(snr_p2[s,nrep]))


		# NO STOKES REGULARIZATION  
		search_space = {"x":tune.grid_search([x]),"t":tune.grid_search([t]),"y":tune.grid_search([y]),"lambdax": tune.grid_search((0.1)**np.linspace(5,15,7)), "lambdaS":tune.grid_search([0.0]) , "beta1":tune.grid_search([0.0]),"beta2":tune.grid_search([0.0]),"sigma2":tune.grid_search([sigma**2])}


		config = param_search(objective_STIS,search_space)
		
		model = STIS(t,y,lambdax=config["lambdax"],lambdaS=config["lambdaS"],beta1=config["beta1"],beta2=config["beta2"],sigma2=sigma**2,p=2)
		optimizer = torch.optim.Adam(model.parameters(), lr=0.01)
		model = optimize_loop(model,optimizer,numit=1000)


		snr_nostokes[s,nrep] = (snr_bivariate(x,model.X.detach().numpy()))

		print("sigma: " + str(sigma) + " nrep: " + str(nrep) + " STIS SNR: "+  str(snr_nostokes[s,nrep]))


		# ONLY SMOOTH STOKES
		search_space = {"x":tune.grid_search([x]),"t":tune.grid_search([t]),"y":tune.grid_search([y]),"lambdax": tune.grid_search([0.0]), "lambdaS":  tune.grid_search((0.1)**np.linspace(5,10,7)) , "beta1":tune.grid_search((0.1)**np.linspace(-2,2,7)),"beta2":tune.grid_search((0.1)**np.linspace(-2,2,5)),"sigma2":tune.grid_search([sigma**2])}

		config = param_search(objective_STIS,search_space)
		
		model = STIS(t,y,lambdax=config["lambdax"],lambdaS=config["lambdaS"],beta1=config["beta1"],beta2=config["beta2"],sigma2=sigma**2,p=2)
		optimizer = torch.optim.Adam(model.parameters(), lr=0.01)
		model = optimize_loop(model,optimizer,numit=1000)


		snr_onlystokes[s,nrep] = (snr_bivariate(x,model.X.detach().numpy()))

		print("sigma: " + str(sigma) + " nrep: " + str(nrep) + " STIS SNR: "+  str(snr_onlystokes[s,nrep]))

		# Kernel regression on normalized 
		search_space = {"x":tune.grid_search([x]),"t":tune.grid_search([t]),"y":tune.grid_search([y]),"alpha": tune.grid_search((0.1)**np.linspace(5,15,7)),"lambda_1": tune.grid_search((0.1)**np.linspace(5,15,7)), "lambda_s":  tune.grid_search((0.1)**np.linspace(5,10,7)) , "beta":tune.grid_search((0.10)**np.linspace(-2,2,7)),"gamma":tune.grid_search([0.2]),"sigma2":tune.grid_search([sigma**2])}
		config = param_search(objective_KReSP,search_space)

		model = KReSP(t,y,lambda_1=config["lambda_1"],beta=config["beta"],lambda_s=config["lambda_s"],alpha=config["alpha"],gamma=config["gamma"],eps=10**-7,win_width=32,sigma2=sigma**2)

		optimizer = torch.optim.Adam(model.parameters(), lr=0.01)
		model = optimize_loop(model,optimizer,numit=300)

		snr_kerreg[s,nrep] = (snr_bivariate(x,model.X.detach().numpy()))

		print("sigma: " + str(sigma) + " nrep: " + str(nrep) + " KReSP SNR: "+  str(snr_kerreg[s,nrep]))

		s+=1 

	with open('results.pkl', 'wb') as f:
		pickle.dump(snr_noise,f)
		pickle.dump(snr_p2,f)
		pickle.dump(snr_nostokes,f)
		pickle.dump(snr_onlystokes,f)
		pickle.dump(snr_kerreg,f)
