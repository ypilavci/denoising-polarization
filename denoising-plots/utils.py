import torch
import numpy as np
import quaternion
from ray import train, tune
from ray.tune.experimental.output import AirVerbosity, get_air_verbosity 
from matplotlib import pyplot as plt
import bispy as bsp

# MODELS
## Prototyping STIS by pytorch 
class STIS(torch.nn.Module):
    def __init__(self,t,y, device='cpu', lambdax=0.0,lambdaS=0.0,beta1=0.0,beta2=0.0,sigma2=1.0,p=2):
        super().__init__()

        # Set parameters
        self.dt = torch.tensor(np.diff(t))
        self.lambdax = lambdax
        self.lambdaS = lambdaS
        self.beta1 = beta1
        self.beta2 = beta2
        self.sigma2 = sigma2
        self.p = p
        self.N = len(y)

        # Generate the quaternion embedding
        self.y = torch.tensor(y).to(device)
        ffty = torch.fft.fft(self.y,dim=0)
        ffty[1:((self.N)//2),:] *= 2.0
        ffty[(self.N//2):self.N,:] = 0.0 + 0.0j

        y_as = torch.fft.ifft(ffty,dim=0)
        self.yquad = torch.cat((y_as.real, y_as.imag),1)

        # Initialize the optimization parameter
        self.X = torch.nn.Parameter(torch.tensor(y).to(device))   

        # Compute the Stokes of the measurements
        self.Sy = torch.tensor(np.zeros([self.N,4]))
        self.Sy[:,0] = self.yquad[:,0]**2 + self.yquad[:,1]**2 + self.yquad[:,2]**2 + self.yquad[:,3]**2
        self.Sy[:,1] = self.yquad[:,0]**2 - self.yquad[:,1]**2 + self.yquad[:,2]**2 - self.yquad[:,3]**2
        self.Sy[:,2] = 2*( self.yquad[:,0]* self.yquad[:,1] + self.yquad[:,2]*self.yquad[:,3] )
        self.Sy[:,3] = 2*( self.yquad[:,1]* self.yquad[:,2] - self.yquad[:,0]*self.yquad[:,3] )

    def forward(self):
        fidelity = torch.norm(self.X.T - self.y.T)**2
        
        # Generate the quaternion embedding
        fftX = torch.fft.fft(self.X,dim=0)
        fftX[1:self.N//2,:] *= 2.0
        fftX[(self.N//2):self.N,:] = 0.0 + 0.0j

        Xhilbert = torch.fft.ifft(fftX,dim=0)
        self.Xquad = torch.cat((Xhilbert.real, Xhilbert.imag),1)
        
        ## INSTANTANEOUS STOKES PARAMETERS COMPUTED 
        self.Sx = torch.tensor(np.zeros([self.N,4]))
 
        self.Sx[:,0] = self.Xquad[:,0]**2 + self.Xquad[:,1]**2 + self.Xquad[:,2]**2 + self.Xquad[:,3]**2
        self.Sx[:,1] = self.Xquad[:,0]**2 - self.Xquad[:,1]**2 + self.Xquad[:,2]**2 - self.Xquad[:,3]**2
        self.Sx[:,2] = 2*( self.Xquad[:,0]* self.Xquad[:,1] + self.Xquad[:,2]*self.Xquad[:,3] )
        self.Sx[:,3] = 2*( self.Xquad[:,1]* self.Xquad[:,2] - self.Xquad[:,0]*self.Xquad[:,3] )       

        fidelity_stokes = self.beta1*(torch.norm( self.Sx[:,1:3] -self.Sy[:,1:3],self.p )**self.p) + self.beta2*(torch.sum(self.Sx[:,0]/(2*self.sigma2) + 0.5*torch.log(self.Sx[:,0])  - torch.log( torch.special.i1e(torch.sqrt(self.Sx[:,0]*self.Sy[:,0])/(self.sigma2)) )  -torch.sqrt(self.Sx[:,0]*self.Sy[:,0])/(self.sigma2)) ) 

        velS = torch.diff(self.Sx,dim=0).T / self.dt 
        velX = torch.diff(self.X.T) / self.dt
        reg1 = self.lambdaS*torch.sum(torch.norm(velS,dim=1)**2)
        reg2 = self.lambdax*(torch.norm(velX)**2)
	
        return (fidelity/self.N + reg1) + (fidelity_stokes/self.N + reg2)

class KReSP(torch.nn.Module):
    def __init__(self,t,y, device='cpu',lambda_1=1,beta=0.1,lambda_s=0.1,alpha=0.1,gamma=1.0,eps=10**-7,win_width=32,sigma2=0.01):
        super().__init__()

        self.dt = torch.tensor(np.diff(t))
        self.sigma2 = sigma2
        self.beta=beta
        self.lambda_s = lambda_s
        self.lambda_1 = lambda_1 
        self.alpha = alpha
        self.gamma = gamma 
        self.t = torch.unsqueeze(torch.tensor(t),1)
        self.N = len(y)
        self.win_width = win_width
        # Generate the quaternion embedding
        self.y = torch.tensor(y).to(device)
        ffty = torch.fft.fft(self.y,dim=0)
        ffty[1:((self.N)//2),:] *= 2.0
        ffty[(self.N//2):self.N,:] = 0.0 + 0.0j

        y_as = torch.fft.ifft(ffty,dim=0)
        self.yquad = torch.cat((y_as.real, y_as.imag),1)

        # Initialize the optimization parameter
        self.X = torch.nn.Parameter(torch.tensor(np.random.randn(self.N,2)).to(device))   

        # Compute the Stokes of the data
        self.Sy = torch.tensor(np.zeros([self.N,4]))
        self.Sy[:,0] = self.yquad[:,0]**2 + self.yquad[:,1]**2 + self.yquad[:,2]**2 + self.yquad[:,3]**2
        self.Sy[:,1] = self.yquad[:,0]**2 - self.yquad[:,1]**2 + self.yquad[:,2]**2 - self.yquad[:,3]**2
        self.Sy[:,2] = 2*( self.yquad[:,0]* self.yquad[:,1] + self.yquad[:,2]*self.yquad[:,3] )
        self.Sy[:,3] = 2*( self.yquad[:,1]* self.yquad[:,2] - self.yquad[:,0]*self.yquad[:,3] )

        # Compute the normalized parameters 
        self.SnY = torch.empty(self.N,3)
        self.SnY[:,0] = self.Sy[:,1]/ self.Sy[:,0]
        self.SnY[:,1] = self.Sy[:,2]/ self.Sy[:,0]
        self.SnY[:,2] = self.Sy[:,3]/ self.Sy[:,0]
        
        self.X = torch.nn.Parameter(torch.tensor(y).to(device))   
        # The Gaussian kernel
        self.K = torch.exp(-torch.cdist(self.t[self.win_width-self.win_width//2:self.win_width+self.win_width//2],self.t[self.win_width-self.win_width//2:self.win_width+self.win_width//2])/(2*(self.gamma**2)))/(self.gamma*np.sqrt(2*torch.pi))
        self.eps = eps
    def forward(self):
        fidelity = (torch.norm(self.X.T - self.y.T)**2)
        velX = torch.diff(self.X.T)/self.dt

        ## HILBERT TRANSFORM COMPUTED IN SPECTRAL DOMAIN 
        fftX = torch.fft.fft(self.X,dim=0)
        fftX[1:(self.N//2),:] *= 2.0
        fftX[(self.N//2):self.N,:] = 0.0 + 0.0j

        Xhilbert = torch.fft.ifft(fftX,dim=0)
        self.Xquad = torch.cat((Xhilbert.real, Xhilbert.imag),1)
        ## INSTANTANEOUS STOKES PARAMETERS COMPUTED 
        self.Sx = torch.tensor(np.zeros([self.N,4]))
 
        self.Sx[:,0] = self.Xquad[:,0]**2 + self.Xquad[:,1]**2 + self.Xquad[:,2]**2 + self.Xquad[:,3]**2
        self.Sx[:,1] = self.Xquad[:,0]**2 - self.Xquad[:,1]**2 + self.Xquad[:,2]**2 - self.Xquad[:,3]**2
        self.Sx[:,2] = 2*( self.Xquad[:,0]* self.Xquad[:,1] + self.Xquad[:,2]*self.Xquad[:,3] )
        self.Sx[:,3] = 2*( self.Xquad[:,1]* self.Xquad[:,2] - self.Xquad[:,0]*self.Xquad[:,3] )       

        # FIDELITY FUNCTION FOR S0
        fidelity_S0 = self.beta*(torch.sum(self.Sx[:,0]/(2*self.sigma2) + 0.5*torch.log(self.Sx[:,0]  - torch.log( torch.special.i1e(torch.sqrt(self.Sx[:,0]*self.Sy[:,0])/(self.sigma2)) ) ) -torch.sqrt(self.Sx[:,0]*self.Sy[:,0])/(self.sigma2) )) 
        velS0 = torch.diff(self.Sx[:,0])/self.dt

        # COMPUTING NORMALIZED STOKES PARAMS 
        SnX = torch.empty(self.N,3)
        SnX[:,0] = self.Sx[:,1] / self.Sx[:,0] 
        SnX[:,1] = self.Sx[:,2] / self.Sx[:,0]  
        SnX[:,2] = self.Sx[:,3] / self.Sx[:,0]     

        # COMPUTING THE KERNEL COST FUNCTION HERE 
        D = torch.zeros(1)
        if(self.alpha >0.0):
            for i in range(self.win_width//2,self.N-self.win_width//2,self.win_width):
                D += torch.sum( (self.Sx[i-self.win_width//2:i+self.win_width//2,0]*self.K)*self.K*(torch.acos(torch.clamp(torch.matmul(SnX[i-self.win_width//2:i+self.win_width//2,:], self.SnY[i-self.win_width//2:i+self.win_width//2,:].T),-1+self.eps,1-self.eps))))                     
        else:
            D= 0.0
        
        kerreg_loss = self.alpha*(D)
        reg2 = self.lambda_1*(torch.norm(velX)**2)
        reg3 = self.lambda_s*(torch.norm(velS0)**2)

        return (fidelity/self.N  + fidelity_S0/self.N + kerreg_loss +reg2 + reg3)
    
# HELPER FUNCTIONS
def optimize_loop(model,optimizer,numit=1000): 
    loss_curve = []
    for e in range(numit):
        l = model()
        l.backward()
        optimizer.step()
        optimizer.zero_grad()
        loss_curve.append(l.item())
    return model 

def snr_bivariate(s_true,s_noisy):
    MSE = 10*np.log10(np.mean(np.linalg.norm(s_true-s_noisy))**2)
    MEAN = 10*np.log10(np.mean(np.linalg.norm(s_true))**2)
    return MEAN - MSE 

def param_search(objective,search_space):

    tuner = tune.Tuner( 
        objective,
        tune_config=tune.TuneConfig(
            metric="snr",
            mode="max",
        ),
        run_config=train.RunConfig(
            verbose = 0,
            stop={"training_iteration": 1},
        ),
        param_space=search_space,
    )
    results = tuner.fit()
    return results.get_best_result().config


def objective_STIS(config):
    model = STIS(config["t"],config["y"],lambdax=config["lambdax"],lambdaS=config["lambdaS"],beta1=config["beta1"],beta2=config["beta2"],sigma2=config["sigma2"])
    optimizer = torch.optim.Adam(model.parameters(), lr=0.01)

    while True:
        model = optimize_loop(model,optimizer,numit=1000)
        X_STIS = model.X.detach().numpy()
        snr = snr_bivariate(config["x"],X_STIS)
        train.report({"snr": snr})  # Report to Tune

def objective_KReSP(config):
    model = KReSP(config["t"],config["y"],lambda_1=config["lambda_1"],beta=config["beta"],lambda_s=config["lambda_s"],alpha=config["alpha"],gamma=config["gamma"],eps=10**-7,sigma2=config["sigma2"])

    optimizer = torch.optim.Adam(model.parameters(), lr=0.01)

    while True:
        model = optimize_loop(model,optimizer,numit=300)
        X_KReSP = model.X.detach().numpy()
        snr = snr_bivariate(config["x"],X_KReSP)
        train.report({"snr": snr})  # Report to Tune



def plot_on_sphere(x_quad,ax,label="",t=[0],scatter=False):

    if(len(t)==1):
        t= np.zeros(len(x_quad))
    S = np.norm(x_quad) + bsp.utils.StokesNorm(x_quad)
    S0 = quaternion.as_float_array(S)[:,0]
    S1 = quaternion.as_float_array(S)[:,2]
    S2 = quaternion.as_float_array(S)[:,3]
    S3 = quaternion.as_float_array(S)[:,1]

    u = np.linspace(0, 2 * np.pi, 20)
    v = np.linspace(0, np.pi, 20)
    a = 1 * np.outer(np.cos(u), np.sin(v))
    b = 1 * np.outer(np.sin(u), np.sin(v))
    c = 1 * np.outer(np.ones(np.size(u)), np.cos(v))
    # Plot the surface
    ax.plot_surface(a,b,c,alpha=0.051)
    ax.set_xlabel("$s_1(t)$")
    ax.set_ylabel("$s_2(t)$")
    ax.set_zlabel("$s_3(t)$")    
    if(scatter):
        ax.scatter(S1/S0,S2/S0,S3/S0,label=label,c=t,s=10,zorder=1)
    else:
        ax.plot(S1/S0,S2/S0,S3/S0,label=label,linewidth=6,color="red",alpha=0.5,zorder=100)


