import numpy as np
import scipy
import matplotlib.pyplot as plt
import quaternion  # load the quaternion module
import bispy as bsp
import torch
from utils import STIS,optimize_loop,snr_bivariate,param_search,objective_STIS,objective_KReSP,KReSP,plot_on_sphere
from ray import  tune,init
import pickle


init(num_cpus=16)

## PLOT AN EXAMPLE 

np.random.seed(5)
N = 1024 # length of the signal
t = np.linspace(0, 2*np.pi/4, N) # time vector
dt = t[1]-t[0]

# ellipse parameters - AM-FM-PM polarized 
theta1 = np.pi/4 - 2*t
chi1 = np.pi/16 - t
phi1 = 0 
f0 = 25/N/dt 
S0 = bsp.utils.windows.hanning(N)

x_quad = bsp.signals.bivariateAMFM(S0, theta1, chi1, 2*np.pi*f0*t+ phi1)
x = quaternion.as_float_array(x_quad)[:,:2]


bsp.utils.visual.plot3D(t,x_quad)
plt.savefig("clean_sig.pdf")
plt.close()

sigma = 0.1
n = np.zeros([N,4])
noise_complex = np.random.randn(N,2)
y = x +sigma*noise_complex

uH = np.imag(scipy.signal.hilbert(noise_complex[:,0]))
vH = np.imag(scipy.signal.hilbert(noise_complex[:,1]))
n[:,0] = noise_complex[:,0]
n[:,1] = noise_complex[:,1]
n[:,2] = uH
n[:,3] = vH
n = quaternion.from_float_array(n)
y_quad = sigma*n + x_quad # Noisy signal

ax = plt.figure().add_subplot(projection='3d')
# ax.view_init(15,-135,0)
plt.title("Normalized Stokes Parameters")
plt.tight_layout()
plot_on_sphere(y_quad,ax,label="noisy signal",t=t,scatter=True)
plot_on_sphere(x_quad,ax,label="original signal",scatter=False)

plt.legend()
plt.tight_layout()
plt.savefig("noisy_sig_sphere.pdf")
plt.close()

bsp.utils.visual.plot3D(t,y_quad)
plt.savefig("noisy_sig.pdf")


print("sigma: " + str(sigma) + " Noise SNR: "+  str(snr_bivariate(x,y) ) )
search_space = {"x":tune.grid_search([x]),"t":tune.grid_search([t]),"y":tune.grid_search([y]),"lambdax": tune.grid_search((0.1)**np.linspace(5,15,7)), "lambdaS":  tune.grid_search((0.1)**np.linspace(5,10,7)) , "beta1":tune.grid_search((0.10)**np.linspace(-2,2,5)),"beta2":tune.grid_search((0.10)**np.linspace(-2,2,5)),"sigma2":tune.grid_search([sigma**2])}
config = param_search(objective_STIS,search_space)
model = STIS(t,y,lambdax=config["lambdax"],lambdaS=config["lambdaS"],beta1=config["beta1"],beta2=config["beta2"],sigma2=sigma**2,p=2)
optimizer = torch.optim.Adam(model.parameters(), lr=0.01)
model = optimize_loop(model,optimizer,numit=1000)

x_stis = quaternion.from_float_array(model.Xquad.detach().numpy())
bsp.utils.visual.plot2D(t,x_stis)
plt.savefig("denoised_via_all_terms.pdf")

print("sigma: " + str(sigma) + " STIS SNR: "+  str(snr_bivariate(x,model.X.detach().numpy())))


# NO STOKES REGULARIZATION  
search_space = {"x":tune.grid_search([x]),"t":tune.grid_search([t]),"y":tune.grid_search([y]),"lambdax": tune.grid_search((0.1)**np.linspace(5,15,7)), "lambdaS":tune.grid_search([0.0]) , "beta1":tune.grid_search([0.0]),"beta2":tune.grid_search([0.0]),"sigma2":tune.grid_search([sigma**2])}
config = param_search(objective_STIS,search_space)
model = STIS(t,y,lambdax=config["lambdax"],lambdaS=config["lambdaS"],beta1=config["beta1"],beta2=config["beta2"],sigma2=sigma**2,p=2)
optimizer = torch.optim.Adam(model.parameters(), lr=0.01)
model = optimize_loop(model,optimizer,numit=1000)

print("sigma: " + str(sigma) + " STIS SNR: "+  str((snr_bivariate(x,model.X.detach().numpy()))))
x_nostokes = quaternion.from_float_array(model.Xquad.detach().numpy())
bsp.utils.visual.plot2D(t,x_nostokes)
plt.savefig("denoised_via_no_stokes.pdf")

# ONLY SMOOTH STOKES
search_space = {"x":tune.grid_search([x]),"t":tune.grid_search([t]),"y":tune.grid_search([y]),"lambdax": tune.grid_search([0.0]), "lambdaS":  tune.grid_search((0.1)**np.linspace(5,10,7)) , "beta1":tune.grid_search((0.1)**np.linspace(-2,2,7)),"beta2":tune.grid_search((0.1)**np.linspace(-2,2,5)),"sigma2":tune.grid_search([sigma**2])}
config = param_search(objective_STIS,search_space)
model = STIS(t,y,lambdax=config["lambdax"],lambdaS=config["lambdaS"],beta1=config["beta1"],beta2=config["beta2"],sigma2=sigma**2,p=2)
optimizer = torch.optim.Adam(model.parameters(), lr=0.01)
model = optimize_loop(model,optimizer,numit=1000)
x_onlystokes = quaternion.from_float_array(model.Xquad.detach().numpy())
bsp.utils.visual.plot2D(t,x_onlystokes)
plt.savefig("denoised_via_no_signal_smoother.pdf")

print("sigma: " + str(sigma) +  " STIS SNR: "+  str((snr_bivariate(x,model.X.detach().numpy()))))

# Kernel regression on normalized 
search_space = {"x":tune.grid_search([x]),"t":tune.grid_search([t]),"y":tune.grid_search([y]),"alpha": tune.grid_search((0.1)**np.linspace(5,15,7)),"lambda_1": tune.grid_search((0.1)**np.linspace(5,15,7)), "lambda_s":  tune.grid_search((0.1)**np.linspace(5,10,7)) , "beta":tune.grid_search((0.10)**np.linspace(-2,2,7)),"gamma":tune.grid_search([0.2]),"sigma2":tune.grid_search([sigma**2])}
config = param_search(objective_KReSP,search_space)
model = KReSP(t,y,lambda_1=config["lambda_1"],beta=config["beta"],lambda_s=config["lambda_s"],alpha=config["alpha"],gamma=config["gamma"],eps=10**-7,win_width=32,sigma2=sigma**2)
optimizer = torch.optim.Adam(model.parameters(), lr=0.01)
model = optimize_loop(model,optimizer,numit=300)


print("sigma: " + str(sigma) + " KReSP SNR: "+  str(snr_bivariate(x,model.X.detach().numpy())))
x_kerreg = quaternion.from_float_array(model.Xquad.detach().numpy())
bsp.utils.visual.plot2D(t,x_kerreg)
plt.savefig("denoised_via_kerreg.pdf")

with open('example.pkl', 'wb') as f:
	pickle.dump(x,f)
	pickle.dump(y,f)
	pickle.dump(x_stis,f)
	pickle.dump(x_nostokes,f)
	pickle.dump(x_onlystokes,f)
	pickle.dump(x_kerreg,f)
